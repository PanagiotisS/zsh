# Setup terminal, and turn on colors
export TERM=xterm-256color
export CLICOLOR=1

# For mac
# export LSCOLORS=Gxfxcxdxbxegedabagacad

# Enable color in grep
export GREP_COLORS='mt=3;33'

# This resolves issues install the mysql, postgres, and other gems with native non universal binary extensions
export ARCHFLAGS='-arch x86_64'

# export LESS='--ignore-case --raw-control-chars'
export PAGER='less'
export EDITOR='nvim'

# export SHELL=/usr/bin/zsh

#export PYTHONPATH=/usr/local/lib/python2.6/site-packages
# CTAGS Sorting in VIM/Emacs is better behaved with this in place
export LC_COLLATE=C

# Do not change prompt when activating virtual environment
export VIRTUAL_ENV_DISABLE_PROMPT=1

# GitHub token with no scope, used to get around API limits
#export HOMEBREW_GITHUB_API_TOKEN=$(cat ~/.gh_api_token)

# export BROWSER="firefox"
# export DEFAULT_USER="username"

# Local bin path
# export PATH=$HOME/.local/bin/:$PATH

# Intel
# source /opt/intel/compilers_and_libraries/linux/bin/compilervars.sh intel64
# source /opt/intel/impi/5.1.2.150/bin64/mpivars.sh
# source /opt/intel/parallel_studio_xe_2016.1.056/bin/psxevars.sh intel64
# source /opt/intel/parallel_studio_xe_2016.1.056/bin/psxevars.sh intel64 mps_hardware_events=yes
#
# CUDA
# export PATH=$PATH:/usr/local/cuda/bin
# export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH

# PGI
# export PGI=/opt/pgi;
# export PATH=/opt/pgi/linux86-64/2016/bin:$PATH;
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/pgi/linux86-64/2016/lib/
# export MANPATH=$MANPATH:/opt/pgi/linux86-64/2016/man;
## License
# export LM_LICENSE_FILE=$LM_LICENSE_FILE:/opt/pgi/license.dat;
# lmgrd.rc {start|stop|restat}

# PGI OpenMPi
# export PATH=$PATH:/opt/pgi/linux86-64/2016/mpi/openmpi/bin;
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/pgi/linux86-64/2016/mpi/openmpi/lib/

# Allinea
# export PATH=$PATH:/opt/allinea/forge/bin/

# TexLive Install
# export PATH=$PATH:/usr/local/texlive/2015/bin/x86_64-linux/

# My Bin
# export PATH=$HOME/bin/:$PATH

# Neovim
# export PATH=$HOME/neovim/bin:$PATH

# GO
# export GOPATH=$HOME/go
# export PATH=$PATH:$GOPATH/bin

# Software Collections RH
# Python 36
# source /opt/rh/rh-python36/enable
# We need the 'corrected' scl_source which is in $HOME/bin atm
# So source after $HOME/bin is exported
# source scl_source enable rh-python36

# Local bin path
# Needed for pip. Export after sourcing rh-python3
# export PATH=$HOME/.local/bin/:$PATH
