# zmodload zsh/zprof
export ZGEN_AUTOLOAD_COMPINIT=0
# Plugin manager
source "${HOME}/.zgenom/zgenom.zsh"
#
# Check if there's no init script
if ! zgenom saved; then

    zgenom load "zsh-users/zaw"
    zgenom load "zsh-users/zsh-syntax-highlighting"
    zgenom load "zsh-users/zsh-autosuggestions"
    zgenom load "zsh-users/zsh-history-substring-search"
    zgenom load "zsh-users/zsh-completions"
    zgenom load "joel-porquet/zsh-dircolors-solarized"
    setupsolarized dircolors.ansi-light

    # Save all to init script
    zgenom save

    # Compile your zsh files
    zgenom compile "$HOME/.zshrc"
    zgenom compile "$HOME/.zsh"
    zgenom compile "$HOME/.zcompdump"

fi

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=90'

fpath=(~/.zsh/comp/ $fpath)

autoload -U ~/.zsh/comp/*(:t)

source ~/.zsh/colors.zsh
source ~/.zsh/setopt.zsh
source ~/.zsh/exports.zsh
source ~/.zsh/prompt.zsh
source ~/.zsh/completion.zsh
source ~/.zsh/aliases.zsh
source ~/.zsh/bindkeys.zsh
source ~/.zsh/functions.zsh
source ~/.zsh/history.zsh
# source ~/.zsh/zsh_hooks.zsh
source ~/.zsh/hashdirs.zsh
source ~/.zsh/autoload.zsh

# Set gnome-terminal background and foreground colours
profile="$(dconf list /org/gnome/terminal/legacy/profiles:/ | sed 's./..')"
if [ $(date +"%H") -ge 6 ] && [ $(date +"%H") -lt 17 ]; then  # solarized-light
    dconf write "/org/gnome/terminal/legacy/profiles:/$profile/foreground-color" "'rgb(101,123,131)'"
    dconf write "/org/gnome/terminal/legacy/profiles:/$profile/background-color" "'rgb(253,246,227)'"
else  # solarized-dark
    dconf write "/org/gnome/terminal/legacy/profiles:/$profile/foreground-color" "'rgb(131,148,150)'"
    dconf write "/org/gnome/terminal/legacy/profiles:/$profile/background-color" "'rgb(0,43,54)'"
fi
