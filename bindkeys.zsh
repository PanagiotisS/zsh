# To see the key combo you want to use just do:
# cat > /dev/null
# And press it
# Or ^V and then press it
#
# Updates editor information when the keymap changes.
function zle-keymap-select() {
  zle reset-prompt
  zle -R
}

# Ensure that the prompt is redrawn when the terminal size changes.
# TRAPWINCH() {
  # zle &&  zle -R
# }

zle -N zle-keymap-select
zle -N edit-command-line
#
bindkey -e   # Default to standard emacs bindings, regardless of editor string
#bindkey -v   # vi mode
export KEYTIMEOUT=1

autoload -U edit-command-line

#bindkey "^K"      kill-whole-line                      # ctrl-k
# bindkey "^R"     history-incremental-search-backward  # ctrl-r
bindkey '^R'       zaw-history
bindkey "^A"       beginning-of-line                    # ctrl-a
bindkey "^E"       end-of-line                          # ctrl-e
bindkey "^[[B"     history-substring-search-down        # down arrow
bindkey "^[[A"     history-substring-search-up          # up arrow
bindkey "^D"       delete-char                          # ctrl-d
bindkey "^[[3~"    delete-char                          # ctrl-d
bindkey "^F"       forward-char                         # ctrl-f
bindkey "^B"       backward-char                        # ctrl-b

bindkey "^P" history-substring-search-up
bindkey "^N" history-substring-search-down

# Edit line wiht $EDITOR
bindkey -M emacs '^xe' edit-command-line
bindkey -M vicmd 'v' edit-command-line

# VI cmd mode
bindkey -M vicmd "^P" history-substring-search-up
bindkey -M vicmd "k"  history-substring-search-up
bindkey -M vicmd "^N" history-substring-search-down
bindkey -M vicmd "j"  history-substring-search-down
bindkey -M vicmd '^R' zaw-history
bindkey -M vicmd "/"  zaw-history
bindkey -M vicmd "^A" beginning-of-line
bindkey -M vicmd "^E" end-of-line
