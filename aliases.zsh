# Pass aliased to sudo
# https://wiki.archlinux.org/index.php/Sudo#Passing_aliases
alias sudo='sudo '

# Colorize output, add file type indicator, and put sizes in human readable format
alias ls='ls -Fh --color=auto'
alias ll='ls -l'
alias l='ls -rlt'
alias l.='ls -ld .*'

# Do NOT store rm commands if `setopt HIST_IGNORE_SPACE` is set
alias rm=' rm'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

alias open='xdg-open'
alias clc='clear; ll'

alias cp='cp -i'
alias mc='mv -i'
alias mv='mv -i'

alias grep='grep --color=auto'
alias ag='ag -i --numbers --nogroup --ignore "./tags"'
alias rg='rg -i --glob "!tags" --no-heading'
alias hist='history | grep'

alias diffs='colordiff -yw -s --suppress-common-lines'
alias ctagsfortran='ctags --language-force=fortran --fortran-kinds=-l -R --fields=nksSaf'

alias jupyterToPDF="jupyter nbconvert --to pdf --template=latex_nocode.tplx '--TemplateExporter.template_path=[\"~/bin\"]'"

alias pip3upgrade="pip3 list --outdated --format=columns | awk 'NR>2 {print $1}' | xargs -n1 pip3 install -U --user"

alias makej='make -j 16'

# ZSH global aliases
alias -g C='| cut -c1-80'
