# Install

```bash
$ git clone git@gitlab.com:PanagiotisS/zsh.git "${HOME}/.zsh"  
$ ln -s ~/.zsh/zshrc ~/.zshrc 
$ git clone https://github.com/tarjoilija/zgen.git "${HOME}/.zgen"  
```

* Exit and re-enter zsh  
