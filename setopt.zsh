# ===== Basics

# If you type foo, and it isn't a command, and it is a directory in your cdpath, go there
setopt AUTO_CD

# Allow comments even in interactive shells (especially for Muness)
setopt INTERACTIVE_COMMENTS

# ===== History

# Allow multiple terminal sessions to all append to one zsh command history
setopt APPEND_HISTORY

# Add comamnds as they are typed, don't wait until shell exit
setopt INC_APPEND_HISTORY

# Do not write events to history that are duplicates of previous events
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_EXPIRE_DUPS_FIRST

# When searching history don't display results already cycled through twice
setopt HIST_FIND_NO_DUPS

# Remove extra blanks from each command line being added to history
setopt HIST_REDUCE_BLANKS

# Include more information about when the command was executed, etc
setopt EXTENDED_HISTORY

# ===== Completion

# Allow completion from within a word/phrase
setopt COMPLETE_IN_WORD

# When completing from the middle of a word, move the cursor to the end of the word
setopt ALWAYS_TO_END

# ===== Prompt

# Enable parameter expansion, command substitution, and arithmetic expansion in the prompt
setopt PROMPT_SUBST

unsetopt MENU_COMPLETE
setopt AUTO_MENU

# If a pattern for filename generation has no matches, print an error, instead of leaving it unchanged in the argument list.
setopt NOMATCH

# Report the status of background jobs immediately, rather than waiting until just before printing a prompt.
setopt NOTIFY

# Do not beep on error
unsetopt beep

# AWESOMENESS
setopt EXTENDED_GLOB

# ===== Cd history
# Limit the directory stack size
DIRSTACKSIZE=10

# Make cd push the old directory onto the directory stack
## Keep history
setopt AUTO_PUSHD

# Exchanges the meanings of ‘+’ and ‘-’ when used with a number to specify a directory in the stack.
## '-' gets the most recent directory and '+' the olest directory first in list
setopt PUSHD_MINUS

# don't push dups on stack
setopt pushdignoredups
